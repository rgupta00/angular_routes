import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deptlist',
  template: `
      deptlist component

      <ul>
      <li  (click)="onSelect(department)" *ngFor="let department of departments">
        <span>{{department.id}}</span> {{department.name}}
      </li>
    </ul>
  `,
  styles: []
})
export class DeptlistComponent implements OnInit {
 //by this code router is going to be DI into this component
  constructor(private router: Router) {

  }

  public departments = [
    {"id": 1, "name": "Angular"},
    {"id": 2, "name": "Node"},
    {"id": 3, "name": "MongoDB"},
    {"id": 4, "name": "Ruby"},
    {"id": 5, "name": "Bootstrap"}
  ]

  // dept/3
  onSelect(department){
    this.router.navigate(['/dept', department.id]);
  }

  ngOnInit() {
  }

}
