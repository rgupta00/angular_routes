import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeptDetailComponent } from './dept-detail/dept-detail.component';
import { DeptlistComponent } from './deptlist/deptlist.component';
import { EmplistComponent } from './emplist/emplist.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


const routes: Routes = [
  { path : '', redirectTo: '/dept', pathMatch: 'full'},
  {path:'emp', component: EmplistComponent},
  {path:'dept', component: DeptlistComponent},
  { path : 'dept/:id', component: DeptDetailComponent},
  {path:'**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
