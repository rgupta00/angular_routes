import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router, ParamMap} from '@angular/router'

@Component({
  selector: 'app-dept-detail',
  template: `
    <p>
      dept-detail of {{deptId}}
    </p>
  `,
  styles: []
})
export class DeptDetailComponent implements OnInit {

  public deptId: number=0;
  //hey angular pl do DI of activateRoute, (so that i can capture variable part of the req :id)
  constructor( private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.deptId =parseInt(this.activateRoute.snapshot.paramMap.get("id"));
  }

}
